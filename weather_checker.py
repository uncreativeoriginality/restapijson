import requests
import json

#curl "https://api.weather.gov/points/39.1137,-76.7267"
#curl https://api.weather.gov/gridpoints/LWX/106,82/forecast

def main():
    try:
        counter = 0
        while (counter < 10):
            ret = requests.get("https://api.weather.gov/gridpoints/LWX/106,82/forecast")
            if (ret.status_code == 200):
                break

            time.sleep(5)
            counter +=1

    except HTTPError as herr:
        print("Http error has occurred: {}".format(herr))
        return
    except Exception as err:
        print("An error has occurred: {}".format(err))

    forecast = json.loads(ret.text)

    print(forecast["properties"]["periods"][0]['detailedForecast'])


main()
