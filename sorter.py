import requests
import json

def main():
    try:
        counter = 0
        while (counter < 10):
            ret = requests.get("https://api.publicapis.org/entries")
            if (ret.status_code == 200):
                break
            
            time.sleep(5)
            counter +=1

    except HTTPError as herr:
        print("Http error has occurred: {}".format(herr))
        return
    except Exception as err:
        print("An error has occurred: {}".format(err))


    apis = json.loads(ret.text)

    categories = dict()
    for index in range(apis["count"]-1, -1, -1):
        if apis["entries"][index]["Auth"]:
            #del apis["entries"][apis.index[item]]
            del apis["entries"][index]
            continue
        if apis["entries"][index]["Category"] not in categories.keys():
            categories[apis["entries"][index]["Category"]] = 1
        else:
            categories[apis["entries"][index]["Category"]] += 1
    
    print(json.dumps(apis, indent=4, sort_keys=True))
    
    for item in sorted(categories.keys()):
        print("{0:<30}".format(item) + "\t{}".format(categories[item]))



main()
