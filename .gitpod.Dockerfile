FROM gitpod/workspace-full

RUN sudo apt update \ 
	&& sudo apt upgrade -y \
	&& sudo apt install -y \
		python3 \
		netcat \
		curl \ 
		wget \
        socat \
		python3-pip \
		golang-go \
	&& pip3 install pandas\
	
	

